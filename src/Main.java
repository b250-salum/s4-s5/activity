import com.zuitt.example.*;


public class Main {
    public static void main(String[] args) {
        Phonebook phonebook = new Phonebook();

        Contact contact1 = new Contact("John Doe ", "+639152468596", "Quezon City");
        Contact contact2 = new Contact("Jane Doe", "+639162148573", "Caloocan City");
        Contact contact3 = new Contact("Ben Doe", "No contact", "Makati City");
        Contact contact4 = new Contact("Ayel Doe", "+639191234567", "No address");

        phonebook.setContacts(contact1);
        phonebook.setContacts(contact2);
        phonebook.setContacts(contact3);
        phonebook.setContacts(contact4);

        if (phonebook.getContacts().isEmpty()) {
            System.out.println("Phonebook is empty.");
        } else {
            for (Contact contact : phonebook.getContacts()) {
                printInfo(contact);
            }
        }
    }

    public static void printInfo(Contact contact) {
        System.out.println("-----------------------");
        System.out.println(contact.getName());
        System.out.println("-----------------------");

        if (contact.getPhoneNumber() != null) {
            System.out.println(contact.getName() + " has the following registered number:");
            System.out.println(contact.getPhoneNumber());
        } else {
            System.out.println(contact.getName() + " has no registered number.");
        }

        if (contact.getAddress() != null) {
            System.out.println(contact.getName() + " has the following registered address:");
            System.out.println(contact.getAddress());
        } else {
            System.out.println(contact.getName() + " has no registered address.");
        }
    }
}
